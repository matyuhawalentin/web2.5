<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf(
        '<style>.bodyContainer{display:none;}</style>
      <div>Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.</div>',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass'])
      );
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);

  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя или убедитесь что нет лишних символов</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните Email или убедитесь в правильности заполнения Email</div>';
  }
  if ($errors['biography']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('biography_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Введите биографию или убедитесь что в тексте нет лишних симовлов</div>';
  }
  if ($errors['year']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('year_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Корректно введите дату рождения</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['year_value'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  // TODO: аналогично все поля.

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (
    empty($errors) && !empty($_COOKIE[session_name()]) &&
    session_start() && !empty($_SESSION['login'])
  ) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio']) || (preg_match("/^[a-z0-9_-]{2,20}$/i", $_POST['fio']))) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['year']) || $_POST['year'] < 1886 || $_POST['year'] > 2021) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('year_value', $_POST['year'], time() + 24 * 60 * 60);
  }
  if (empty($_POST['email']) || !preg_match("/[0-9a-z]+@[a-z]/", $_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  // *************
  // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
  // Сохранить в Cookie признаки ошибок и значения полей.
  // *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  } 
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('year_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (
    !empty($_COOKIE[session_name()]) &&
    session_start() && !empty($_SESSION['login'])
  ) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    $userBD = 'u24334';
  $passBD = '9864761';
  $db = new PDO('mysql:host=localhost;dbname=u24334', $userBD, $passBD, array(PDO::ATTR_PERSISTENT => true));
    try {
      $str = implode(',', $_POST['sp-sp']);

      $stmt = $db->prepare("UPDATE user SET fio = ?, email = ?, yob = ?, pol = ?, limb = ?, biograghy = ?");
      $stmt->execute([$_POST['fio'], $_POST['email'], $_POST['year'], $_POST['radio-pol'], $_POST['radio-kon'], $_POST['biography']]);

      $stmt = $db->prepare("UPDATE abilities SET abilities = ?");
      $stmt->execute([$str]);
    } catch (PDOException $e) {
      print('Error : ' . $e->getMessage());
      exit();
    }
    header('Location: ./');
  } 
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $logins = (string)rand();
    $pass = (string)rand();
    $passMD5=md5($pass);
    // Сохраняем в Cookies.
    setcookie('login', $logins);
    setcookie('pass', $pass);
    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...
    $userBD = 'u24334';
    $passBD = '9864761';
    $db = new PDO('mysql:host=localhost;dbname=u24334', $userBD, $passBD, array(PDO::ATTR_PERSISTENT => true));
      try {
        $str = implode(',', $_POST['sp-sp']);

        $stmt = $db->prepare("INSERT INTO user SET fio = ?, logins = ?, pass = ?, email = ?, yob = ?, pol = ?, limb = ?, biograghy = ?");
        $stmt->execute([$_POST['fio'], $logins, $passMD5, $_POST['email'], $_POST['year'], $_POST['radio-pol'], $_POST['radio-kon'], $_POST['biography']]);

        $stmt = $db->prepare("INSERT INTO abilities SET abilities = ?");
        $stmt->execute([$str]);
  
      } catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
      }
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    // Делаем перенаправление.
    header('Location: ./');
  }
}
